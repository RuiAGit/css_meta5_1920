INSERT INTO Discount (id, DTYPE, description) VALUES (1, 'NoDiscount', 'Sem desconto');
INSERT INTO Discount (id, DTYPE, description) VALUES (2, 'ThresholdPercentageDiscount', 'Percentagem do Total (acima de limiar)');
INSERT INTO Discount (id, DTYPE, description) VALUES (3, 'EligibleProductsDiscount', 'Percentagem do Total Elegivel');
